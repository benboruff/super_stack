defmodule SuperStack.MixProject do
  use Mix.Project

  def project do
    [
      app: :super_stack,
      version: "0.1.0",
      elixir: "~> 1.9",
      description: description(),
      package: package(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      source_url: "https://gitlab.com/benboruff/super_stack"
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {SuperStack.Application, []},
      registered: [SuperStack.Server]
    ]
  end

  defp deps do
    [
      {:cupboard, "~> 0.2.0"},
      {:ex_doc, "~> 0.21.1", only: :dev, runtime: false}
    ]
  end

  defp description do
    "A supervised stack server."
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/benboruff/super_stack"}
    ]
  end
end
