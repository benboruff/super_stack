defmodule SuperStack.Server do
  @doc false
  use GenServer

  @stack __MODULE__

  @moduledoc """
  Documentation for `SuperStack.Server` API.

  `SuperStack.Server` maintains a list of values
  as state. You can `push` and `pop` values to
  and from state.

  The current state can be obtained
  via `show`.

  State persists via a `Cupboard.Server` in a separate
  process. `SuperStack.Server` gets it's state from the
  `Cupboard.Server`.

  `SuperStack.Server`'s state is a list.

  `SuperStack.Server` is supervised. If it fails it
  will store it's current state in the `Cupboard.Server`
  and restart using the stored state.
  """

  #######
  # API #
  #######

  @doc false
  def start_link(state), do: GenServer.start_link(@stack, state, name: @stack)

  @doc """
  `push` an element onto the `SuperStack.Server`

  ## Example

      iex>SuperStack.Server.reset
      :ok
      iex>SuperStack.Server.push 1
      :ok
      iex>SuperStack.Server.show
      [1]

  """
  def push(item), do: GenServer.cast(@stack, {:push, item})

  @doc """
  `pop` an element off of the `SuperStack.Server`

  ## Example

      iex>SuperStack.Server.reset
      :ok
      iex>SuperStack.Server.push 1
      :ok
      iex>SuperStack.Server.show
      [1]
      iex>SuperStack.Server.pop
      1

  """
  def pop, do: GenServer.call(@stack, :pop)

  @doc """
  `show` the `SuperStack.Server`'s current state

  ## Example

      iex>SuperStack.Server.reset
      iex>SuperStack.Server.show
      []

  """
  def show, do: GenServer.call(@stack, :show)

  @doc """
  `reset` the `SuperStack.Server`'s state to an empty list.

  ## Example

      iex>SuperStack.Server.reset
      iex>SuperStack.Server.push 1
      :ok
      iex>SuperStack.Server.show
      [1]
      iex>SuperStack.Server.reset
      :ok
      iex>SuperStack.Server.show
      []

  """
  def reset, do: GenServer.cast(@stack, :reset)

  ##################
  # Implementation #
  ##################

  @doc false
  def init(_) do
    {:ok, Cupboard.Server.get()}
  end

  def handle_cast({:push, item}, current_stack) do
    {:noreply, [item | current_stack]}
  end

  def handle_cast(:reset, _current_stack) do
    {:noreply, []}
  end

  def handle_call(:pop, _from, []) do
    {:reply, nil, []}
  end

  def handle_call(:pop, _from, current_stack) do
    [head | tail] = current_stack
    {:reply, head, tail}
  end

  def handle_call(:show, _from, current_stack) do
    {:reply, current_stack, current_stack}
  end

  @doc false
  def format_status(_reason, [_pdict, state]) do
    [data: [{'State', "My current state is '#{inspect(state)}', and I feel fine!"}]]
  end

  def terminate(_reason, current_stack) do
    Cupboard.Server.update(current_stack)
  end
end
