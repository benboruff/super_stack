defmodule SuperStack.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # for the stack, initial state will be a list
      {Cupboard.Server, []},
      SuperStack.Server
    ]

    opts = [strategy: :rest_for_one, name: SuperStack.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
